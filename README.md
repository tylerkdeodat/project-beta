# CarCar

Team:

* Ben Perlman - Service API/Poller
* Tyler Deodat - Sales API/Poller

## Design
There are three bounded contexts: Service, Sales, and Inventory. CarCar uses REST APIs and a django framework in order to show data for the frontends ,using React JS, of the microservices: Sales/Service/Inventory.
## Service microservice

Explain your models and integration with the inventory
microservice, here.

 I made three models: The simplest model, Technician, had name and ID fields for the employee. AutomobileVO has a vin field and an href field. I redefined the name to be the vin as part of highlighting VIP customers. The service model is most complicated and had fields for VIN, owner, appointment reason, appointment date, boolean finished and boolean VIP. It also had a foreign key reference to technician. Along the way I made the VIN field not unique because a car could be service more than once and removed defualtautonow from the date field because the appointment isn' ncessarily now.



 I integrated with the inventory micro service in order to highlight the VIP through the view function. I polled the inventory API in order to give automobile VO VIN data. Then when the service form is submitted, I check if there's an object in automobileVO with a matching vin. If there's a match, VIP is set to true, otherwise the try except block fails and VIP is set to false. Then the service object is sent to the API to be saved.


## Sales microservice

The Sales Microservice has four models: AutomobileVO, SalesPerson, Customer, and SalesRecord.

The AutomobileO model that has 5 fields: VIN, color, year, href, and sold. Due to the fact that once an automobile gets sold the sold value changes from the default of false to true which also stops it from populating on any list that a customer can see as it is no longer for sale. The vin property has a unique = true which allows us to use that to filter by vin but also stops multiple copies of the same automobile fro being created. The AutomobileVO polls data from the Automobile model in the inventory microservice.

The SalesPerson model only takes two fields: name and employee number. The employee number property is also unique so that way two employees do not have the same number and we cansort and filter by each individual employee. 

The Customer model takes three fields: name, address, and phone number.

The SaleRecord model takes four fields: price, customer, sales_person, automobile. 
the customer field is a foreign key to the Customer model. sales_person has a foreign key to the SalesPerson model and automobile has a foreign key to the AutomobileVO model. This way when creating a recordof a automobile being sold. The data for the customer and sales person and automobile is pulled in to populate the sales record. Then once the recod is created the sold field on automobile is changed from a default of false to true through a put request and is refelcted in both the AutomobileVO model in sales microservice and Automobile model in the inventory microservice through a get request to list all the automobiles.  
