import React, { Component } from 'react'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalesPersonForm from './SalesPersonForm';
import CustomerForm from './CustomerForm';
import SalesRecordForm from './SalesRecordForm';
import AppointmentList from './AppointmentList';
import TechForm from './TechForm';
import ServiceForm from './ServiceForm';
import ServiceHistoryList from './ServiceHistoryList';
import SalesRecordList from './SalesRecordList';
import ManufacturersList from './ManufacturersList';
import ManufacturersForm from './ManufacturersForm';
import AutoList from './AutoList';
import AutoForm from './AutoForm';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelsForm';
import SalesByEmployee from './SalesByEmployee';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    Promise.all([
      fetch('http://localhost:8090/api/sales_record/'),
      fetch('http://localhost:8080/api/services/'),
      fetch('http://localhost:8090/api/sales_person/'),
      fetch('http://localhost:8100/api/models/'),
      fetch('http://localhost:8100/api/manufacturers/'),
      fetch('http://localhost:8100/api/automobiles/'),
    ])
      .then(([sales_records, services, sales_person, models, manufacturers, automobiles]) => {
        return Promise.all([
          sales_records.json(),
          services.json(),
          sales_person.json(),
          models.json(),
          manufacturers.json(),
          automobiles.json(),
        ])
      })
      .then(
        ([sales_records, services, sales_person, models, manufacturers, automobiles]) => {
          this.setState(sales_records);
          this.setState(services);
          this.setState(sales_person);
          this.setState(models);
          this.setState(manufacturers);
          this.setState(automobiles);
        }
      )
  }

  render() {
    return (
      <BrowserRouter>
        <Nav />
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="sales_person/">
              <Route path="new/" element={<SalesPersonForm />} />
              <Route path="" element={<SalesByEmployee />} />
            </Route>
            <Route path="customer/">
              <Route path="new/" element={<CustomerForm />} />
            </Route>
            <Route path="sales_record/">
              <Route path="new/" element={<SalesRecordForm />} />
              <Route path="" element={<SalesRecordList sale_records={this.state.sale_records} />} />
            </Route>
            <Route path="models/">
              <Route path="new/" element={<VehicleModelForm />} />
              <Route path="" element={<VehicleModelList vehicleModels={this.state.models} />} />
            </Route>


          <Route path="manufacturers">
            <Route index element={<ManufacturersList manufacturers={this.state.manufacturers} />} />
            <Route path="new" element={<ManufacturersForm />} />
          </Route>

          <Route path="autos">
            <Route index element={<AutoList autos={this.state.autos} />} />
            <Route path="new" element={<AutoForm />} />
          </Route>

          <Route path="appointments">
            <Route index element={<AppointmentList services={this.state.services} />} />
            <Route path="new" element={<ServiceForm />} />
            <Route path="history" element={<ServiceHistoryList />} />
        </Route>

        <Route path="tech">
          <Route path="new" element={<TechForm />} />
        </Route>


            
          </Routes>
        </div>
      </BrowserRouter>
    );
  }
}