import React from 'react';

class ServiceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vin: "",     
      owner: '',
      reason: "",
      date: "2022-1-1 12:30",
      technicians:[],
 
    }
//vin name date/time reason tech
    this.handleSubmit = this.handleSubmit.bind(this);

    this.handleChangeOwner = this.handleChangeOwner.bind(this);
    this.handleChangeVIN = this.handleChangeVIN.bind(this);
    this.handleChangeTechnician = this.handleChangeTechnician.bind(this);
    this.handleChangeReason = this.handleChangeReason.bind(this);
    this.handleChangeDate = this.handleChangeDate.bind(this);

  }

  

 
  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    console.log(data)

    //data.tech_name = data.technicians
    data.appt_reason = data.reason
    data.appt_date = data.date
    
    delete data.technicians;

    delete data.reason;
    delete data.date;

    console.log(data)

    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',

      },
    };
    const url = 'http://localhost:8080/api/services/';
    const request = await fetch(url, fetchOptions);
    if (request.ok) {
      this.setState({
        owner: '',
        vin: "",
        reason: "",
        date: "2022-1-1 12:30",
      });
    }
  }


  


  handleChangeVIN(event) {
    const value = event.target.value;
    this.setState({ vin: value });
  }

  handleChangeOwner(event) {
    const value = event.target.value;
    this.setState({ owner: value });
  }

  handleChangeTechnician(event) {
    const value = event.target.value;
    this.setState({ technician: value });
  }
  handleChangeReason(event) {
    const value = event.target.value;
    this.setState({ reason: value });
  }

  handleChangeDate(event) {
    const value = event.target.value;
    this.setState({ date: value });
  }

  async componentDidMount() {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }

  render() {

    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Service Appointment Record</h1>
            <form onSubmit={this.handleSubmit} id="addTechForm">
              
            <div className="form-floating mb-3">
                <input onChange={this.handleChangeVIN} value={this.state.vin} placeholder="Vin" required type="text" id="vin" className="form-control" />
                <label htmlFor="vin">VIN</label>
              </div>
              
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeOwner} value={this.state.owner} placeholder="Owner" required type="text" id="owner" className="form-control" />
                <label htmlFor="owner">Owner</label>
              </div>
           
            
              <div className="form-floating mb-3">
                <input onChange={this.handleChangeReason} value={this.state.reason} placeholder="Reason" required type="text" id="reason" className="form-control" />
                <label htmlFor="tech_id">Appointment Reason</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={this.handleChangeDate} value={this.state.date} placeholder="Date" required type="text" id="date" className="form-control" />
                <label htmlFor="date">Date</label>
              </div>

            <div className="mb-3">
                    <select onChange={this.handleChangeTechnician} name="Technician" id="technician" className="form-select" required>
                      <option value="">Choose a Tech</option>
                      {this.state.technicians.map(technician => {
                        return (
                          <option key={technician.id} value={technician.id}>{technician.tech_name}</option>
                        )
                      })}
                    </select>
                  </div>


              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceForm;
