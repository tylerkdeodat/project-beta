import React from 'react';

function VehicleModelList({vehicleModels}) {
    return (
      <>
      <h1>Vehicle Models</h1>
        <table className="table table-hover table-dark table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Manufacturer</th>
                <th>Picture</th>
            </tr>
            </thead>
            <tbody>
                {vehicleModels && vehicleModels.map(models => {
                    return (
                    <tr key={models.id}>
                        <td>{ models.name}</td>
                        <td>{ models.manufacturer.name }</td>
                        <td><img alt = 'car' src={ models.picture_url } width="80%" height="auto"/></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
      </>
    );
  }
  
  export default VehicleModelList;