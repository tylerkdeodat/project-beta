from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder

from .models import  Service, Technician, AutomobileVO

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "tech_id",
        "tech_name",
        "id",
    ]

class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "id"
,        "vin",
        "owner",
        "appt_reason",
        "appt_date",
        "technician",
        "finished",
        "vip",

    ]
    encoders = {"technician": TechnicianEncoder()}

@require_http_methods(["DELETE", "GET", "PUT"])
def api_service(request, pk):
    if request.method == "GET":
        try:
            services = Service.objects.get(pk=pk)
            return JsonResponse(
                services,
                encoder=ServiceEncoder,
                safe=False
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            services = Service.objects.get(pk=pk)
            services.delete()
            return JsonResponse(
                services,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            services = Service.objects.get(pk=pk)

     
            setattr(services, "finished", True)
            services.save()
            return JsonResponse(
                services,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response



@require_http_methods(["GET", "POST"])
def api_services(request):
    if request.method == "GET":
       # services = Service.objects.all()
        services = Service.objects.exclude(finished=True)
        return JsonResponse(
            {"services": services},
            encoder=ServiceEncoder,
        )
    else:
        print("bad")
        content = json.loads(request.body)
        print(content)
        try:
            content = json.loads(request.body)
            print(content)
           
            if "technician" in content:
                technician = Technician.objects.get(id=content["technician"])
                content["technician"] = technician
            try: 
                AutomobileVO.objects.get(vin=content["vin"])
                vip =  True
            except:
                vip = False
            content["vip"] = vip   


            service = Service.objects.create(**content)
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the service appointment."}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "Post"])
def api_services_list(request,pk):
    if request.method == "GET":
        services = Service.objects.get(id=pk)
        return JsonResponse(
            services,
            encoder=ServiceEncoder,
            safe = False,
        )

@require_http_methods(["GET"])
def api_service_history(request,pk):

        services = Service.objects.filter(vin=pk)
        print(services)
        return JsonResponse(
            {"services": services},
            encoder=ServiceEncoder,
            safe = False,
        )
 