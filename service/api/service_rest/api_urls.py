from django.urls import path

from .api_views import api_service, api_service_history, api_services, api_technicians  #api_list_attendees, api_show_attendee

urlpatterns = [
    path("technicians/", api_technicians, name="api_create_technician"),
    path("services/", api_services, name="api_create_service"),
      path(
        "services/<int:pk>/",
        api_service,
        name="api_service",
    ),
    path("services/history/<str:pk>/", api_service_history, name="api_service_history"),
]

