# Generated by Django 4.0.3 on 2022-06-22 23:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0003_service_finished'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='vin',
            field=models.CharField(max_length=17),
        ),
    ]
