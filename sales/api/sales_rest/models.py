
from re import T
from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    color = models.CharField(max_length=50, null=True, blank=True)
    year = models.PositiveSmallIntegerField(blank=True, null=True)
    href = models.CharField(max_length=250, blank=True, null=True)
    sold = models.BooleanField(default=False)
 

class SalesPerson(models.Model):
    name = models.CharField(max_length=75)
    employee_number = models.PositiveSmallIntegerField(unique=True)
        
    def __str__(self):
        return self.name


class Customer(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=11)

    def __str__(self):
        return self.name


class SalesRecord(models.Model):
    price = models.CharField(max_length=10)
    customer = models.ForeignKey(
        Customer,
        related_name="sales_record",
        on_delete=models.PROTECT,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_person",
        on_delete=models.PROTECT,
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.PROTECT,
        blank=False,
        null=False,
    )
